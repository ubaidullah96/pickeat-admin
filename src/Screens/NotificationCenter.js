import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  Dimensions,
  Platform,
  PixelRatio,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import Ripple from '../Components/Ripple';
// import {
//   getNotifications,
//   updateNotification,
//   getNotificationCount,
// } from '../Services/DataManager';
import {
  getAllNotifications,
  updateNotification,
} from '../Services/API/APIManager';

const {width: SCREEN_WIDTH} = Dimensions.get('window');
const scale = SCREEN_WIDTH / 320;

const normalize = (size) => {
  const newSize = size * scale;
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
};

const NotificationCenter = ({navigation}) => {
  useEffect(() => {
    // return navigation.addListener('focus', async () => {
    //   const res = await fetchAllNotifications();
    //   if (res && res.data && res.data.notifications) {
    //     navigation.setParams({
    //       notificationCount: res.data.notifications.filter(
    //         (n) => n.is_read === 0,
    //       ).length,
    //     });
    //   }
    // });
  });

  const fetchAllNotifications = async () => {
    const res = await getAllNotifications();
    console.log(res.data);
    if (res && res.data && res.data.notifications) {
      navigation.setParams({
        notificationCount: res.data.notifications.filter((n) => n.is_read === 0)
          .length,
      });
      setNotifications(res.data.notifications);
    }
  };

  useEffect(() => {
    fetchAllNotifications();
    // fetchNotifications();
  }, []);

  const [notifications, setNotifications] = useState([]);

  // const fetchNotifications = () => {
  //   getNotifications().then((notifications) => {
  //     if (notifications) {
  //       const sortedNotifications = notifications.sort(
  //         (a, b) => a.time < b.time,
  //       );
  //       setNotifications(sortedNotifications);
  //       getNotificationCount().then((notificationCount) =>
  //         navigation.setParams({notificationCount}),
  //       );
  //     }
  //   });
  // };

  const onPressNotification = async (notification) => {
    // await updateNotification(notification.time, {
    //   ...notification,
    //   isSeen: !notification.isSeen,
    // });
    // fetchNotifications();
    const data = {
      notification_id: notification.id,
      is_read: 1,
    };
    await updateNotification(data);
    fetchAllNotifications();
    if (notification && notification.type) {
      switch (notification.type) {
        case 'pay-by-cash':
          if (notification.table_id) {
            navigation.navigate('Payment', {
              tableId: notification.table_id,
            });
          }
          break;
        case 'reservation':
          navigation.navigate('Reservations');
          break;
      }
    }
  };

  return (
    <View style={{flex: 1}}>
      <FlatList
        style={{paddingVertical: '2%'}}
        data={notifications}
        renderItem={({item, index}) => (
          <Ripple
            style={{
              shadowColor: '#cc0001',
              backgroundColor: '#fff',
              shadowOpacity: 0.3,
              shadowRadius: 4.65,
              shadowOffset: {width: 0, height: 4},
              elevation: 5,
              padding: '3%',
              marginHorizontal: '3.5%',
              marginVertical: '1%',
              marginBottom: index === notifications.length - 1 ? '8%' : '1%',
              borderRadius: 5,
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={() => onPressNotification(item)}>
            <View style={{width: '85%', paddingHorizontal: '2%'}}>
              <Text
                style={{
                  fontSize: normalize(18),
                  color: '#757575',
                  fontWeight: 'bold',
                }}>
                {item.type}
              </Text>
              <Text style={{color: '#555', fontSize: normalize(16)}}>
                {item.message}
              </Text>
              <Text style={{color: '#999', fontSize: normalize(15)}}>
                {moment(item.created_at).format('MM/DD/YYYY hh:mm:ss a')}
              </Text>
            </View>
            <View style={{width: '20%', alignItems: 'center'}}>
              <MaterialIcon
                style={{marginRight: 10}}
                name="notifications"
                size={25}
                color={item.is_read ? '#555' : '#fe0000'}
              />
            </View>
          </Ripple>
        )}
      />
    </View>
  );
};

export default NotificationCenter;
